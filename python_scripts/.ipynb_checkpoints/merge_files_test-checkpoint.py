import argparse
import pandas as pd
import os
import warnings
warnings.filterwarnings('ignore')

# notes:
# be able to submit using sbatch
# want to be able to break up that file of zipcodes
# reading in the list of zipcodes and breaking it up over some increment
# submit these increments in different jobs
# last job would take the written out files and merge them together
# around 10-30 mins per increment

# do this for years too (for both options)
# document functions with docstrings
# mkdocs to write up a little thing about what this does and we can package it
# or write a page or two so we can put it out to the community

def merge_files_list(input_file):
    directory_path = "/hpc/group/borsuklab/at341/infousa_copy/2020"
    if not os.path.exists(directory_path):
        print("This directory does not exist")
        return
    if type(directory_path) != str:
        print("Please input the directory path as a string")
        return
    os.chdir(directory_path)
    directory = os.listdir(directory_path)
    
    # now split it up into sections of 1000 files
    with open(input_file, 'r') as f:
        num_zipcodes = len(fp.readlines())
    
    if num_zipcodes <= 1000:
        
    
    # reading in the input text file and formatting file names
    with open(input_file) as f:
               zipcode_list = ["Household_Ethnicity_zip_" + line.rstrip('\n') + "_year_2020.txt" for line in f]
    
    # only keeping the files for zipcodes which are in the InfoUSA dataset
    files = set(directory).intersection(set(zipcode_list))
    
        
    print(zipcode_list)
    print(files)
    
    # merging files    
    df_merged = pd.concat([pd.read_csv(f, sep = '\t', 
                                       dtype = {'census_state_2010': 'str', 'census_county_2010': 'str', 
                                               'BOX_NUM': 'str'}) for f in files])
    
    # writing to parquet file
    df_merged.to_csv("/hpc/group/borsuklab/at341/test31.csv", index = False)

def get_args_parse():

    # Create the parser
    my_parser = argparse.ArgumentParser(description='Parsing arguments for script')

    # Add the arguments
    my_parser.add_argument('file_path',
                           type=str,
                           help='the entire text file\'s path')
    
    # Execute the parse_args() method
    args = my_parser.parse_args()
    return args

if __name__ == '__main__':
    input_file = get_args_parse().file_path
    merge_files_list(input_file)
    