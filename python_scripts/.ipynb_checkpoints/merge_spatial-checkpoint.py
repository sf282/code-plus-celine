import argparse
import pandas as pd
import geopandas as gpd
import os
import warnings
warnings.filterwarnings('ignore')
  
def merge_files_list(zipcode_list):
    directory_path = "/hpc/group/borsuklab/at341/infousa_copy/2020" 
    # adding in an argument to be the directory path for the entire infousa dataset
    # all the options are in one file with different arguments
    if not os.path.exists(directory_path):
        print("This directory does not exist")
        return
    if type(directory_path) != str:
        print("Please input the directory path as a string")
        return
    os.chdir(directory_path)
    directory = os.listdir(directory_path)
    
    formatted_list = []
    # formatting file names
    for i in range(len(zipcode_list)):
        formatted_list.append("Household_Ethnicity_zip_" + str(zipcode_list[i]) + "_year_2020.txt")
    
    # only keeping the files which are in the InfoUSA database
    files = set(directory).intersection(set(formatted_list))
    
    # print(formatted_list)
    # print(directory)
    # print(files)

    df_merged = pd.concat([pd.read_csv(f, sep = '\t', 
                                       dtype = {'census_state_2010': 'str', 'census_county_2010': 'str',
                                                'BOX_NUM': 'str', 'HOUSE_NUM': 'str', 'UNIT_NUM': 'str', 
                                                'ROUTE_NUM': 'str', 'Ethnicity_Code_3': 'str',
                                               'Ethnicity_Code_1': 'str'}) for f in files])
    
    df_merged.to_parquet("/hpc/group/borsuklab/at341/test19.parquet", index = False)
    
def merge_files_spatial(directory_path):
    infousa_directory_path = "/hpc/group/borsuklab/at341/infousa_copy/2020"
    spatial_directory_path = directory_path
    zipcode_directory_path = "/hpc/group/borsuklab/at341/celine_data/source_files/zipcode_shapefiles/zipcodes.shp" # pass this in as an argument
    # store zipcode data in the dcc
    
    # would need to have this in the dcc directory
    print(spatial_directory_path)
    # checking if these files exist
    if not os.path.exists(infousa_directory_path):
        print("The InfoUSA directory does not exist")
        return
    if not os.path.exists(zipcode_directory_path):
        print("The zipcode directory does not exist")
        return
    if not os.path.exists(spatial_directory_path):
        print("The directory you inputted does not exist")
        return
    if type(spatial_directory_path) != str:
        print("Please input the directory path as a string")
        return
    
    # loading dataframes
    df_zipcodes = gpd.read_file("/hpc/group/borsuklab/at341/celine_data/source_files/zipcode_shapefiles/zipcodes.shp")
    df_spatial = gpd.read_file(spatial_directory_path)
    
    # formatting zipcode dataframe
    df_zipcodes = df_zipcodes[['ZCTA5CE20', 'geometry']]
    df_zipcodes.rename(columns = {'ZCTA5CE20': 'zipcode'}, inplace = True)
    df_zipcodes['zipcode'] = df_zipcodes['zipcode'].astype('str')
    
    # first find intersection between dataframes
    df_intersect = gpd.sjoin(df_zipcodes, df_spatial, how = "inner", predicate = "intersects")
    
    df_intersect = df_intersect.drop_duplicates(subset = 'zipcode')
    df_intersect = df_intersect[['zipcode']]

    zipcode_list = df_intersect.zipcode.values.tolist()
    
    merge_files_list(zipcode_list)

def get_args_parse():
    
    # Create the parser
    my_parser = argparse.ArgumentParser(description='Testing!')

    # Add the arguments
    my_parser.add_argument('file_path',
                           type=str,
                           help='a list of words word')
    
    # Execute the parse_args() method
    args = my_parser.parse_args()
    return args

if __name__ == '__main__':
    input_path = get_args_parse().file_path
    merge_files_spatial(input_path)