#!/bin/bash

#SBATCH --job-name=merging_files      ## Name of the job
#SBATCH --output=out.out              ## Output file
#SBATCH --error=out.err
#SBATCH --time=2:00                   ## Job Duration
#SBATCH --ntasks=1                    ## Number of tasks (analyses) to run
#SBATCH --cpus-per-task=1             ## The number of threads the code will use
#SBATCH --mem-per-cpu=1600MB          ## Real memory(MB) per CPU required by the job.

## Load the python interpreter
module load Python/2.7.11

## Execute the python script and pass the argument/input '90'
pip list