#!/bin/bash

#SBATCH --job-name=merging_files      ## Name of the job
#SBATCH --output=out.out              ## Output file
#SBATCH --error=out.err
#SBATCH --time=5:00                   ## Job Duration
#SBATCH --ntasks=1                    ## Number of tasks (analyses) to run
#SBATCH --cpus-per-task=8             ## The number of threads the code will use
#SBATCH --mem-per-cpu=2000MB          ## Real memory(MB) per CPU required by the job.

# Activate conda environment
source activate borsuklab

## Execute the python script and pass the argument/input '90'
python merge_spatial.py /hpc/group/borsuklab/at341/wy_shapefiles/wy_shapefiles.shp