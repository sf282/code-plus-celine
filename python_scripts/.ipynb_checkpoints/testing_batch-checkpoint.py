import argparse
import pandas as pd
import os
import warnings
warnings.filterwarnings('ignore')

# tentative procedure:
# iterate through the first 3000 files
# merge the files and export to parquet file in a new directory 
# then iterate through the next, etc. until end of text file
# after everything is exported, get a list of all filenames in the new directory
# merge those two at a time until you're done

def merge_files_list(input_file):
    directory_path = "/hpc/group/borsuklab/at341/infousa_copy/2020"
    if not os.path.exists(directory_path):
        print("This directory does not exist")
        return
    if type(directory_path) != str:
        print("Please input the directory path as a string")
        return
    os.chdir(directory_path)
    directory = os.listdir(directory_path)

    zipcode_list = []
    
    f = open('/hpc/home/at341/ondemand/code-plus-celine/python_scripts/zipcodes.txt')
    i = 1
    while i <= 3000:
        
    with open(input_file) as f:
        
               zipcode_list = ["Household_Ethnicity_zip_" + line.rstrip('\n') + "_year_2020.txt" for line in f]
    
    # only keeping the files for zipcodes which are in the InfoUSA dataset
    files = set(directory).intersection(set(zipcode_list))
    
    print(files)
    
    # merging files    
    df_merged = pd.concat([pd.read_csv(f, sep = '\t', 
                                       dtype = {'census_state_2010': 'str', 'census_county_2010': 'str'}) for f in files])
    
    # writing to parquet file
    df_merged.to_parquet("/hpc/group/borsuklab/at341/test29.parquet")

def get_args_parse():

    # Create the parser
    my_parser = argparse.ArgumentParser(description='Parsing arguments for script')

    # Add the arguments
    my_parser.add_argument('file_path',
                           type=str,
                           help='the entire text file\'s path')
    
    # Execute the parse_args() method
    args = my_parser.parse_args()
    return args

if __name__ == '__main__':
    input_file = get_args_parse().file_path
    merge_files_list(input_file)