import argparse

def get_word(word):
    return word

def get_args_parse():
    
    # Create the parser
    my_parser = argparse.ArgumentParser(description='Testing!')

    # Add the arguments
    my_parser.add_argument('word',
                           type=str,
                           help='a list of words word')

    # Execute the parse_args() method
    args = my_parser.parse_args()
    return args

if __name__ == '__main__':
    input_word = get_args_parse().word
    print(get_word(input_word))
    # print(get_word(input_word))
    # merge_files_list(zipcode_list)

    # input_word = args.Word

    # print(input_word)