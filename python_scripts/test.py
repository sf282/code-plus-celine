import argparse
import pandas as pd
import geopandas as gpd
import os
import warnings
warnings.filterwarnings('ignore')

def merge_files_textfile(zipcode_file, infousa_path, output_path):
    infousa_path = infousa_path
    
    if not os.path.exists(infousa_path):
        print("The InfoUSA directory path you entered does not exist.")
        return
    
    if type(infousa_path) != str:
        print("Please input the InfoUSA directory path as a string")
        return
    
    os.chdir(infousa_path)
    infousa_directory = os.listdir(infousa_path)
    
    # reading in the input text file and formatting file names
    with open(zipcode_file) as f:
               zipcode_list = ["Household_Ethnicity_zip_" + line.rstrip('\n') + "_year_2020.txt" for line in f]
    
    # only keeping the files for zipcodes which are in the InfoUSA dataset
    files = set(infousa_directory).intersection(set(zipcode_list))
    
    # merging files    
    df_merged = pd.concat([pd.read_csv(f, sep = '\t', 
                                       dtype = {'census_state_2010': 'str', 'census_county_2010': 'str', 
                                               'BOX_NUM': 'str', 'HOUSE_NUM': 'str', 'UNIT_NUM': 'str', 
                                                'ROUTE_NUM': 'str', 'Ethnicity_Code_3': 'str'}) for f in files])
    
    # writing to parquet file
    df_merged.to_parquet(str(output_path) + '/merged_infousa_textfile.parquet', index = False)

def merge_files_list(zipcode_list, infousa_path, output_path):
    infousa_path = infousa_path

    if not os.path.exists(infousa_path):
        print("This directory does not exist.")
        return
    if type(infousa_path) != str:
        print("Please input the directory path as a string.")
        return
    os.chdir(infousa_path)
    infousa_directory = os.listdir(infousa_path)
    
    formatted_list = []
    
    # formatting file names
    for i in range(len(zipcode_list)):
        formatted_list.append("Household_Ethnicity_zip_" + str(zipcode_list[i]) + "_year_2020.txt") # CAN'T HARDCODE YEAR - CHANGE
    
    # only keeping the files which are in the InfoUSA database
    files = set(infousa_directory).intersection(set(formatted_list))

    df_merged = pd.concat([pd.read_csv(f, sep = '\t', 
                                       dtype = {'census_state_2010': 'str', 'census_county_2010': 'str',
                                                'BOX_NUM': 'str', 'HOUSE_NUM': 'str', 'UNIT_NUM': 'str', 
                                                'ROUTE_NUM': 'str', 'Ethnicity_Code_3': 'str',
                                               'Ethnicity_Code_1': 'str'}) for f in files])
        
    df_merged.to_parquet(str(output_path) + '/merged_infousa_spatial.parquet', index = False)
    
def merge_files_spatial(spatial_file, zipcode_file, infousa_path, output_path):
    infousa_path = infousa_path
    spatial_path = spatial_file
    zipcode_path = zipcode_file 
    output_path = output_path
        
    # checking if these files exist
    if not os.path.exists(infousa_path):
        print("The InfoUSA directory path you inputted does not exist.")
        return
    if not os.path.exists(zipcode_path):
        print("The zipcode file path you inputted does not exist.")
        return
    if not os.path.exists(spatial_file):
        print("The spatial file path you inputted does not exist.")
        return
    if type(spatial_path) != str:
        print("Please input the directory path as a string")
        return
    
    # loading dataframes
    df_zipcodes = gpd.read_file(str(zipcode_path))
    df_spatial = gpd.read_file(spatial_path)
    
    # formatting zipcode dataframe
    df_zipcodes = df_zipcodes[['ZCTA5CE20', 'geometry']]
    df_zipcodes.rename(columns = {'ZCTA5CE20': 'zipcode'}, inplace = True)
    df_zipcodes['zipcode'] = df_zipcodes['zipcode'].astype('str')
    
    # first find intersection between dataframes
    df_intersect = gpd.sjoin(df_zipcodes, df_spatial, how = "inner", predicate = "intersects")
    
    df_intersect = df_intersect.drop_duplicates(subset = 'zipcode')
    df_intersect = df_intersect[['zipcode']]

    zipcode_list = df_intersect.zipcode.values.tolist()
    
    merge_files_list(zipcode_list, infousa_path, output_path)

def get_args_parse():
    
    # Create the parser
    my_parser = argparse.ArgumentParser(description='Options for InfoUSA dataset subsetting')

    # Add the arguments
    
    my_parser.add_argument('--infousa', type=str, required=True, 
                           help='a complete path to the InfoUSA dataset')
    
    my_parser.add_argument('--output', type=str, required=True, 
                           help='a complete path to the folder you\'d like your output to be written to')

    my_parser.add_argument('--zipcode_input', type=str, required=False, 
                           help='a complete path to a .txt file with the zipcodes to subset by')
    
    my_parser.add_argument('--spatial', type=str, required=False,
                           help='a complete path to a .shp file with the geometries to subset by')
    
    my_parser.add_argument('--zipcode', type=str, required=False,
                           help='a complete path to a .shp file with zipcode geometries')
    
    # Execute the parse_args() method
    args = my_parser.parse_args()
    
    return args

if __name__ == '__main__':
    
    args = get_args_parse()
    
    if args.zipcode_input and args.spatial:
        print('Please input one or the other, not both.')
        exit() # don't run anything in this case
        
    if args.zipcode_input:        
        zipcode_file = get_args_parse().zipcode_input
        infousa_path = get_args_parse().infousa
        output_path = get_args_parse().output
        
        merge_files_textfile(zipcode_file, infousa_path, output_path)
    else:
        print('No zipcode file path entered')
        
    if args.spatial:
        spatial_file = get_args_parse().spatial
        zipcode_file = get_args_parse().zipcode
        infousa_path = get_args_parse().infousa
        output_path = get_args_parse().output
                
        merge_files_spatial(spatial_file, zipcode_file, infousa_path, output_path)
        
    else:
        print('No spatial file path entered')