#!/bin/bash

#SBATCH --job-name=test      ## Name of the job
#SBATCH --output=out.out              ## Output file
#SBATCH --error=out.err
#SBATCH --time=10:00                   ## Job Duration
#SBATCH --ntasks=1                    ## Number of tasks (analyses) to run
#SBATCH --cpus-per-task=8             ## The number of threads the code will use
#SBATCH --mem-per-cpu=1200MB           ## Real memory(MB) per CPU required by the job. THIS IS THE PERFECT AMOUNT FOR 1000 FILES I CHECKED

# Activate conda environment
source activate borsuklab

## Execute the python script and pass the argument/input '90'
python hello.py