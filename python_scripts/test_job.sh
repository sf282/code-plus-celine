#!/bin/bash

#SBATCH --job-name=merging_files      ## Name of the job
#SBATCH --output=out.out              ## Output file
#SBATCH --error=out.err
#SBATCH --time=10:00                   ## Job Duration
#SBATCH --ntasks=1                    ## Number of tasks (analyses) to run
#SBATCH --cpus-per-task=16             ## The number of threads the code will use
#SBATCH --mem-per-cpu=15000MB           ## Real memory(MB) per CPU required by the job. THIS IS THE PERFECT AMOUNT FOR 1000 FILES I CHECKED

# Activate conda environment
source activate borsuklab

# python test.py --zipcode_input /hpc/home/at341/ondemand/code-plus-celine/python_scripts/zipcodes_test00.txt --infousa /hpc/group/borsuklab/at341/infousa_copy/2020 --output /hpc/group/borsuklab/at341

python test.py --spatial /hpc/group/borsuklab/at341/ny.shp --zipcode /hpc/group/borsuklab/at341/celine_data/source_files/zipcode_shapefiles/zipcodes.shp --infousa /hpc/group/borsuklab/at341/infousa_copy/2020 --output /hpc/group/borsuklab/at341